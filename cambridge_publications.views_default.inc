<?php

/**
 * @file
 * cambridge_publications.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function cambridge_publications_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'publications_by_author';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'elements_instance';
  $view->human_name = 'Publication by author';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'attest';
  $handler->display->display_options['css_class'] = 'campl-content-container';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Elements: pnid */
  $handler->display->display_options['relationships']['pub_nid']['id'] = 'pub_nid';
  $handler->display->display_options['relationships']['pub_nid']['table'] = 'elements_instance';
  $handler->display->display_options['relationships']['pub_nid']['field'] = 'pub_nid';
  $handler->display->display_options['relationships']['pub_nid']['required'] = TRUE;
  /* Relationship: Elements: snid */
  $handler->display->display_options['relationships']['staff_nid']['id'] = 'staff_nid';
  $handler->display->display_options['relationships']['staff_nid']['table'] = 'elements_instance';
  $handler->display->display_options['relationships']['staff_nid']['field'] = 'staff_nid';
  $handler->display->display_options['relationships']['staff_nid']['required'] = TRUE;
  /* Field: Elements: id */
  $handler->display->display_options['fields']['spid']['id'] = 'spid';
  $handler->display->display_options['fields']['spid']['table'] = 'elements_instance';
  $handler->display->display_options['fields']['spid']['field'] = 'spid';
  /* Field: Elements: pnid */
  $handler->display->display_options['fields']['pub_nid']['id'] = 'pub_nid';
  $handler->display->display_options['fields']['pub_nid']['table'] = 'elements_instance';
  $handler->display->display_options['fields']['pub_nid']['field'] = 'pub_nid';
  /* Field: Elements: snid */
  $handler->display->display_options['fields']['staff_nid']['id'] = 'staff_nid';
  $handler->display->display_options['fields']['staff_nid']['table'] = 'elements_instance';
  $handler->display->display_options['fields']['staff_nid']['field'] = 'staff_nid';
  /* Field: Content: Rendered Node */
  $handler->display->display_options['fields']['rendered_entity']['id'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['rendered_entity']['field'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['relationship'] = 'pub_nid';
  $handler->display->display_options['fields']['rendered_entity']['label'] = '';
  $handler->display->display_options['fields']['rendered_entity']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['rendered_entity']['link_to_entity'] = 1;
  $handler->display->display_options['fields']['rendered_entity']['display'] = 'view';
  $handler->display->display_options['fields']['rendered_entity']['view_mode'] = 'teaser';
  $handler->display->display_options['fields']['rendered_entity']['bypass_access'] = 0;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'admin/publicationtest';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No pubs to show.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Rendered Node */
  $handler->display->display_options['fields']['rendered_entity']['id'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['rendered_entity']['field'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['relationship'] = 'pub_nid';
  $handler->display->display_options['fields']['rendered_entity']['label'] = '';
  $handler->display->display_options['fields']['rendered_entity']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['rendered_entity']['link_to_entity'] = 1;
  $handler->display->display_options['fields']['rendered_entity']['display'] = 'view';
  $handler->display->display_options['fields']['rendered_entity']['view_mode'] = 'publication_item';
  $handler->display->display_options['fields']['rendered_entity']['bypass_access'] = 0;
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Elements: snid */
  $handler->display->display_options['arguments']['staff_nid']['id'] = 'staff_nid';
  $handler->display->display_options['arguments']['staff_nid']['table'] = 'elements_instance';
  $handler->display->display_options['arguments']['staff_nid']['field'] = 'staff_nid';
  $handler->display->display_options['arguments']['staff_nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['staff_nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['staff_nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['staff_nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['staff_nid']['summary_options']['items_per_page'] = '25';

  /* Display: Grouped block */
  $handler = $view->new_display('block', 'Grouped block', 'block_2');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Grouped Block';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_pub_type',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
    1 => array(
      'field' => 'field_pub_date_year',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No pubs to show.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Elements: pnid */
  $handler->display->display_options['relationships']['pub_nid']['id'] = 'pub_nid';
  $handler->display->display_options['relationships']['pub_nid']['table'] = 'elements_instance';
  $handler->display->display_options['relationships']['pub_nid']['field'] = 'pub_nid';
  $handler->display->display_options['relationships']['pub_nid']['required'] = TRUE;
  /* Relationship: Elements: snid */
  $handler->display->display_options['relationships']['staff_nid']['id'] = 'staff_nid';
  $handler->display->display_options['relationships']['staff_nid']['table'] = 'elements_instance';
  $handler->display->display_options['relationships']['staff_nid']['field'] = 'staff_nid';
  $handler->display->display_options['relationships']['staff_nid']['required'] = TRUE;
  /* Relationship: Content: Taxonomy terms on node */
  $handler->display->display_options['relationships']['term_node_tid']['id'] = 'term_node_tid';
  $handler->display->display_options['relationships']['term_node_tid']['table'] = 'node';
  $handler->display->display_options['relationships']['term_node_tid']['field'] = 'term_node_tid';
  $handler->display->display_options['relationships']['term_node_tid']['relationship'] = 'pub_nid';
  $handler->display->display_options['relationships']['term_node_tid']['label'] = 'term - publication type';
  $handler->display->display_options['relationships']['term_node_tid']['vocabularies'] = array(
    'publication_type' => 'publication_type',
    'restricted_content' => 0,
    'sd_classification' => 0,
    'sd_departments' => 0,
    'sd_specialities' => 0,
    'subject' => 0,
  );
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Rendered Node */
  $handler->display->display_options['fields']['rendered_entity']['id'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['rendered_entity']['field'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['relationship'] = 'pub_nid';
  $handler->display->display_options['fields']['rendered_entity']['label'] = '';
  $handler->display->display_options['fields']['rendered_entity']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['rendered_entity']['link_to_entity'] = 1;
  $handler->display->display_options['fields']['rendered_entity']['display'] = 'view';
  $handler->display->display_options['fields']['rendered_entity']['view_mode'] = 'publication_item';
  $handler->display->display_options['fields']['rendered_entity']['bypass_access'] = 0;
  /* Field: Content: pub_date_year */
  $handler->display->display_options['fields']['field_pub_date_year']['id'] = 'field_pub_date_year';
  $handler->display->display_options['fields']['field_pub_date_year']['table'] = 'field_data_field_pub_date_year';
  $handler->display->display_options['fields']['field_pub_date_year']['field'] = 'field_pub_date_year';
  $handler->display->display_options['fields']['field_pub_date_year']['relationship'] = 'pub_nid';
  $handler->display->display_options['fields']['field_pub_date_year']['label'] = '';
  $handler->display->display_options['fields']['field_pub_date_year']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_pub_date_year']['element_class'] = 'campl-content-container';
  $handler->display->display_options['fields']['field_pub_date_year']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_pub_date_year']['element_default_classes'] = FALSE;
  /* Field: Content: Publication type */
  $handler->display->display_options['fields']['field_pub_type']['id'] = 'field_pub_type';
  $handler->display->display_options['fields']['field_pub_type']['table'] = 'field_data_field_pub_type';
  $handler->display->display_options['fields']['field_pub_type']['field'] = 'field_pub_type';
  $handler->display->display_options['fields']['field_pub_type']['relationship'] = 'pub_nid';
  $handler->display->display_options['fields']['field_pub_type']['label'] = '';
  $handler->display->display_options['fields']['field_pub_type']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_pub_type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_pub_type']['type'] = 'ds_taxonomy_view_mode';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Taxonomy term: Weight */
  $handler->display->display_options['sorts']['weight']['id'] = 'weight';
  $handler->display->display_options['sorts']['weight']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['sorts']['weight']['field'] = 'weight';
  $handler->display->display_options['sorts']['weight']['relationship'] = 'term_node_tid';
  /* Sort criterion: Content: pub_date_year (field_pub_date_year) */
  $handler->display->display_options['sorts']['field_pub_date_year_value']['id'] = 'field_pub_date_year_value';
  $handler->display->display_options['sorts']['field_pub_date_year_value']['table'] = 'field_data_field_pub_date_year';
  $handler->display->display_options['sorts']['field_pub_date_year_value']['field'] = 'field_pub_date_year_value';
  $handler->display->display_options['sorts']['field_pub_date_year_value']['relationship'] = 'pub_nid';
  $handler->display->display_options['sorts']['field_pub_date_year_value']['order'] = 'DESC';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Elements: snid */
  $handler->display->display_options['arguments']['staff_nid']['id'] = 'staff_nid';
  $handler->display->display_options['arguments']['staff_nid']['table'] = 'elements_instance';
  $handler->display->display_options['arguments']['staff_nid']['field'] = 'staff_nid';
  $handler->display->display_options['arguments']['staff_nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['staff_nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['staff_nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['staff_nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['staff_nid']['summary_options']['items_per_page'] = '25';

  /* Display: EVA Field */
  $handler = $view->new_display('entity_view', 'EVA Field', 'entity_view_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = '<none>';
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_pub_type',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
    1 => array(
      'field' => 'field_pub_date_year',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['empty'] = FALSE;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Elements: pnid */
  $handler->display->display_options['relationships']['pub_nid']['id'] = 'pub_nid';
  $handler->display->display_options['relationships']['pub_nid']['table'] = 'elements_instance';
  $handler->display->display_options['relationships']['pub_nid']['field'] = 'pub_nid';
  $handler->display->display_options['relationships']['pub_nid']['required'] = TRUE;
  /* Relationship: Elements: snid */
  $handler->display->display_options['relationships']['staff_nid']['id'] = 'staff_nid';
  $handler->display->display_options['relationships']['staff_nid']['table'] = 'elements_instance';
  $handler->display->display_options['relationships']['staff_nid']['field'] = 'staff_nid';
  $handler->display->display_options['relationships']['staff_nid']['required'] = TRUE;
  /* Relationship: Content: Taxonomy terms on node */
  $handler->display->display_options['relationships']['term_node_tid']['id'] = 'term_node_tid';
  $handler->display->display_options['relationships']['term_node_tid']['table'] = 'node';
  $handler->display->display_options['relationships']['term_node_tid']['field'] = 'term_node_tid';
  $handler->display->display_options['relationships']['term_node_tid']['relationship'] = 'pub_nid';
  $handler->display->display_options['relationships']['term_node_tid']['label'] = 'term - publication type';
  $handler->display->display_options['relationships']['term_node_tid']['vocabularies'] = array(
    'publication_type' => 'publication_type',
    'restricted_content' => 0,
    'sd_classification' => 0,
    'sd_departments' => 0,
    'sd_specialities' => 0,
    'subject' => 0,
  );
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Rendered Node */
  $handler->display->display_options['fields']['rendered_entity']['id'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['rendered_entity']['field'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['relationship'] = 'pub_nid';
  $handler->display->display_options['fields']['rendered_entity']['label'] = '';
  $handler->display->display_options['fields']['rendered_entity']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['rendered_entity']['link_to_entity'] = 1;
  $handler->display->display_options['fields']['rendered_entity']['display'] = 'view';
  $handler->display->display_options['fields']['rendered_entity']['view_mode'] = 'publication_item';
  $handler->display->display_options['fields']['rendered_entity']['bypass_access'] = 0;
  /* Field: Content: pub_date_year */
  $handler->display->display_options['fields']['field_pub_date_year']['id'] = 'field_pub_date_year';
  $handler->display->display_options['fields']['field_pub_date_year']['table'] = 'field_data_field_pub_date_year';
  $handler->display->display_options['fields']['field_pub_date_year']['field'] = 'field_pub_date_year';
  $handler->display->display_options['fields']['field_pub_date_year']['relationship'] = 'pub_nid';
  $handler->display->display_options['fields']['field_pub_date_year']['label'] = '';
  $handler->display->display_options['fields']['field_pub_date_year']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_pub_date_year']['element_label_colon'] = FALSE;
  /* Field: Content: Publication type */
  $handler->display->display_options['fields']['field_pub_type']['id'] = 'field_pub_type';
  $handler->display->display_options['fields']['field_pub_type']['table'] = 'field_data_field_pub_type';
  $handler->display->display_options['fields']['field_pub_type']['field'] = 'field_pub_type';
  $handler->display->display_options['fields']['field_pub_type']['relationship'] = 'pub_nid';
  $handler->display->display_options['fields']['field_pub_type']['label'] = '';
  $handler->display->display_options['fields']['field_pub_type']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_pub_type']['element_type'] = 'h2';
  $handler->display->display_options['fields']['field_pub_type']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_pub_type']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Taxonomy term: Weight */
  $handler->display->display_options['sorts']['weight']['id'] = 'weight';
  $handler->display->display_options['sorts']['weight']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['sorts']['weight']['field'] = 'weight';
  $handler->display->display_options['sorts']['weight']['relationship'] = 'term_node_tid';
  /* Sort criterion: Content: pub_date_year (field_pub_date_year) */
  $handler->display->display_options['sorts']['field_pub_date_year_value']['id'] = 'field_pub_date_year_value';
  $handler->display->display_options['sorts']['field_pub_date_year_value']['table'] = 'field_data_field_pub_date_year';
  $handler->display->display_options['sorts']['field_pub_date_year_value']['field'] = 'field_pub_date_year_value';
  $handler->display->display_options['sorts']['field_pub_date_year_value']['relationship'] = 'pub_nid';
  $handler->display->display_options['sorts']['field_pub_date_year_value']['order'] = 'DESC';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Elements: snid */
  $handler->display->display_options['arguments']['staff_nid']['id'] = 'staff_nid';
  $handler->display->display_options['arguments']['staff_nid']['table'] = 'elements_instance';
  $handler->display->display_options['arguments']['staff_nid']['field'] = 'staff_nid';
  $handler->display->display_options['arguments']['staff_nid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['staff_nid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['staff_nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['staff_nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['staff_nid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Show publications (field_pub_show_publications) */
  $handler->display->display_options['filters']['field_pub_show_publications_value']['id'] = 'field_pub_show_publications_value';
  $handler->display->display_options['filters']['field_pub_show_publications_value']['table'] = 'field_data_field_pub_show_publications';
  $handler->display->display_options['filters']['field_pub_show_publications_value']['field'] = 'field_pub_show_publications_value';
  $handler->display->display_options['filters']['field_pub_show_publications_value']['relationship'] = 'staff_nid';
  $handler->display->display_options['filters']['field_pub_show_publications_value']['value'] = array(
    1 => '1',
  );
  $handler->display->display_options['entity_type'] = 'node';
  $handler->display->display_options['bundles'] = array(
    0 => 'staff_profile',
  );
  $handler->display->display_options['show_title'] = 1;
  $export['publications_by_author'] = $view;

  $view = new view();
  $view->name = 'publications_search';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Publications Search';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Publication search';
  $handler->display->display_options['css_class'] = 'campl-content-container';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Header: Global: Result summary */
  $handler->display->display_options['header']['result']['id'] = 'result';
  $handler->display->display_options['header']['result']['table'] = 'views';
  $handler->display->display_options['header']['result']['field'] = 'result';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Content: Rendered Node */
  $handler->display->display_options['fields']['rendered_entity']['id'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['rendered_entity']['field'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['label'] = '';
  $handler->display->display_options['fields']['rendered_entity']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['rendered_entity']['link_to_entity'] = 1;
  $handler->display->display_options['fields']['rendered_entity']['display'] = 'view';
  $handler->display->display_options['fields']['rendered_entity']['view_mode'] = 'publication_search_result';
  $handler->display->display_options['fields']['rendered_entity']['bypass_access'] = 0;
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Filter criterion: Content: Published status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'publication' => 'publication',
  );
  /* Filter criterion: Content: Publication type (field_pub_type) */
  $handler->display->display_options['filters']['field_pub_type_tid']['id'] = 'field_pub_type_tid';
  $handler->display->display_options['filters']['field_pub_type_tid']['table'] = 'field_data_field_pub_type';
  $handler->display->display_options['filters']['field_pub_type_tid']['field'] = 'field_pub_type_tid';
  $handler->display->display_options['filters']['field_pub_type_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_pub_type_tid']['expose']['operator_id'] = 'field_pub_type_tid_op';
  $handler->display->display_options['filters']['field_pub_type_tid']['expose']['label'] = 'Publication type';
  $handler->display->display_options['filters']['field_pub_type_tid']['expose']['operator'] = 'field_pub_type_tid_op';
  $handler->display->display_options['filters']['field_pub_type_tid']['expose']['identifier'] = 'field_pub_type_tid';
  $handler->display->display_options['filters']['field_pub_type_tid']['expose']['remember_roles'] = array(
    2 => '2',
  );
  $handler->display->display_options['filters']['field_pub_type_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_pub_type_tid']['vocabulary'] = 'publication_type';
  /* Filter criterion: Content: Title */
  $handler->display->display_options['filters']['title']['id'] = 'title';
  $handler->display->display_options['filters']['title']['table'] = 'node';
  $handler->display->display_options['filters']['title']['field'] = 'title';
  $handler->display->display_options['filters']['title']['operator'] = 'contains';
  $handler->display->display_options['filters']['title']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['label'] = 'Title';
  $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
  $handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
    2 => '2',
  );
  /* Filter criterion: Content: pub_date_year (field_pub_date_year) */
  $handler->display->display_options['filters']['field_pub_date_year_value']['id'] = 'field_pub_date_year_value';
  $handler->display->display_options['filters']['field_pub_date_year_value']['table'] = 'field_data_field_pub_date_year';
  $handler->display->display_options['filters']['field_pub_date_year_value']['field'] = 'field_pub_date_year_value';
  $handler->display->display_options['filters']['field_pub_date_year_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_pub_date_year_value']['expose']['operator_id'] = 'field_pub_date_year_value_op';
  $handler->display->display_options['filters']['field_pub_date_year_value']['expose']['label'] = 'Year';
  $handler->display->display_options['filters']['field_pub_date_year_value']['expose']['operator'] = 'field_pub_date_year_value_op';
  $handler->display->display_options['filters']['field_pub_date_year_value']['expose']['identifier'] = 'field_pub_date_year_value';
  $handler->display->display_options['filters']['field_pub_date_year_value']['expose']['remember_roles'] = array(
    2 => '2',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'publications/search';
  $export['publications_search'] = $view;

  return $export;
}
